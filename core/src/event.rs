/* Copyright 2020 Lucas Hinderberger
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use uuid::Uuid;

#[derive(Clone, Debug, PartialEq)]
pub struct Event {
    pub stream_id: Uuid,
    pub serial: u64,
    pub payload: String
}