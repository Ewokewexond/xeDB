/* Copyright 2020 Lucas Hinderberger
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

//TODO: Maybe split this up into multiple more specialized error types?
#[derive(Debug)]
pub enum Error {
    BincodeError(bincode::Error),
    Conflict,
    DatabaseLocked,
    DatabaseNotFound,
    IOError(std::io::Error),
    PathDoesNotExist,
    SerialSkipped,
    TargetPathNotEmpty
    //This will be completed at a later point in time
}

pub type Result<T> = std::result::Result<T, Error>;

impl From<bincode::Error> for Error {
    fn from(bincode_error: bincode::Error) -> Error {
        Error::BincodeError(bincode_error)
    }
}

impl From<std::io::Error> for Error {
    fn from(io_error: std::io::Error) -> Error {
        Error::IOError(io_error)
    }
}
