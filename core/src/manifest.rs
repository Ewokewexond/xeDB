/* Copyright 2020 Lucas Hinderberger
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

 use serde::{Serialize, Deserialize};
 use std::fs;
 use std::path::Path;
 use super::Error;

 #[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Manifest {
    pub version: u16
}

impl Manifest {
    pub fn save_to_file(&self, path: &Path) -> Result<(), Error> {
        let serialized = bincode::serialize(self)?;
        fs::write(path, serialized)?;

        Ok(())
    }
}