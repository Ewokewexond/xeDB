/* Copyright 2020 Lucas Hinderberger
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use super::{ Error, Event, Result, manifest };
use std::path::{ Path, PathBuf };
use uuid::Uuid;

#[derive(Debug)]
pub struct Database {
    manifest: manifest::Manifest,
    path: PathBuf
}

impl Database {
    pub fn create(path: &Path) -> Result<Self> {
        if !path_is_empty(path)? {
            return Err(Error::TargetPathNotEmpty)
        }

        let manifest = manifest::Manifest{ version: 0 };
        manifest.save_to_file(&path.join("xedb-manifest"))?;

        Self::open(path)
    }

    pub fn open(path: &Path) -> Result<Self> {
        unimplemented!();
    }


    pub fn path(&self) -> &Path {
        &self.path
    }

    pub fn read_stream(&self, stream_id: Uuid) -> Result<Vec<Event>> {
        unimplemented!();
    }

    pub fn write_events(&mut self, events: &[Event]) -> Result<()> {
        unimplemented!();
    }
}

fn path_is_empty(path: &Path) -> std::io::Result<bool> {
    Ok(path.read_dir()?.next().is_none())
}
