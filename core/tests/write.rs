/* Copyright 2020 Lucas Hinderberger
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#[macro_use]
mod assertions;

mod temp_db;

use uuid::Uuid;
use temp_db::create_temp_database;
use xedb_core::Event;

#[test]
fn writing_to_new_stream_succeeds() {
    let mut temp_database = create_temp_database();

    let stream_id = Uuid::parse_str("ed40fd27-54b6-4ca3-8b72-52d602753846").unwrap();
    let events = vec![
        Event{stream_id: stream_id, serial: 0, payload: "Hello World".to_string() },
        Event{stream_id: stream_id, serial: 1, payload: "Another Event...".to_string() },
        Event{stream_id: stream_id, serial: 2, payload: "...and a third one".to_string() }
    ];

    temp_database.database.write_events(&events).unwrap();
}

#[test]
fn writing_to_existing_stream_succeeds() {
    let mut temp_database = create_temp_database();

    let stream_id = Uuid::parse_str("892845a6-a242-408b-93a7-10be46a78d31").unwrap();
    let events_one = vec![
        Event{stream_id: stream_id, serial: 0, payload: "Hello World".to_string() }
    ];
    let events_two = vec![  
        Event{stream_id: stream_id, serial: 1, payload: "Another Event...".to_string() },
        Event{stream_id: stream_id, serial: 2, payload: "...and a third one".to_string() }
    ];

    temp_database.database.write_events(&events_one).unwrap();
    temp_database.database.write_events(&events_two).unwrap();
}

#[test]
fn writing_to_multiple_streams_succeeds() {
    let mut temp_database = create_temp_database();

    let stream_a_id = Uuid::parse_str("ee2c6441-3057-42ae-bcc0-2936006bca1a").unwrap();
    let stream_b_id = Uuid::parse_str("491a7855-c1af-4b67-8ef5-fbdcdef57c7e").unwrap();
    let events = vec![
        Event{stream_id: stream_a_id, serial: 0, payload: "Hello Stream A".to_string() },
        Event{stream_id: stream_b_id, serial: 0, payload: "Hello Stream B".to_string() }
    ];

    temp_database.database.write_events(&events).unwrap();
}

#[test]
fn writing_empty_events_succeeds() {
    let mut temp_database = create_temp_database();

    let stream_id = Uuid::parse_str("40aa1b9b-236a-4901-873d-b75fbd035cf4").unwrap();
    let events = vec![
        Event{stream_id: stream_id, serial: 0, payload: String::new() },
    ];

    temp_database.database.write_events(&events).unwrap();
}

#[test]
fn writing_conflicting_events_yields_error() {
    let mut temp_database = create_temp_database();

    let stream_id = Uuid::parse_str("095406bb-0956-420f-907e-eebfadaf9581").unwrap();
    let existing_events = vec![
        Event{stream_id: stream_id, serial: 0, payload: "Hello World".to_string() },
    ];
    let additional_events = vec![
        Event{stream_id: stream_id, serial: 0, payload: "Conflicting".to_string() }
    ];

    temp_database.database.write_events(&existing_events).unwrap();

    let error = temp_database.database.write_events(&additional_events).unwrap_err();
    assert_discriminant_eq!(&error, &xedb_core::Error::Conflict);
}

#[test]
fn writing_conflicting_events_within_batch_yields_error() {
    let mut temp_database = create_temp_database();

    let stream_id = Uuid::parse_str("091f35d2-2c32-45bb-bfde-865dfbd49d61").unwrap();
    let events = vec![
        Event{stream_id: stream_id, serial: 0, payload: "Something".to_string() },
        Event{stream_id: stream_id, serial: 0, payload: "Something else".to_string() },
    ];

    let error = temp_database.database.write_events(&events).unwrap_err();
    assert_discriminant_eq!(&error, &xedb_core::Error::Conflict);
}

#[test]
fn writing_non_successive_events_yields_error() {
    let mut temp_database = create_temp_database();

    let stream_id = Uuid::parse_str("a1f70464-dd09-4795-83ae-1d4987ce412a").unwrap();
    let events = vec![
        Event{stream_id: stream_id, serial: 0, payload: "Hello World".to_string() },
        Event{stream_id: stream_id, serial: 2, payload: "Another Event...".to_string() },
        Event{stream_id: stream_id, serial: 3, payload: "...and a third one".to_string() }
    ];

    let error = temp_database.database.write_events(&events).unwrap_err();
    assert_discriminant_eq!(&error, &xedb_core::Error::SerialSkipped);
}
