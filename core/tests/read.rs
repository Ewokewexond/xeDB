/* Copyright 2020 Lucas Hinderberger
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

mod temp_db;

use uuid::Uuid;
use temp_db::create_temp_database;
use xedb_core::Event;

#[test]
fn reading_events_yields_complete_unmodified_ordered_event_stream() {
    let mut temp_database = create_temp_database();

    let stream_a_id = Uuid::parse_str("bb43612c-8b35-48ab-ab02-8211a476fbbf").unwrap();
    let stream_b_id = Uuid::parse_str("674c73fe-c344-456f-8ed1-69e97bf596cc").unwrap();
    let events_a = vec![
        Event{stream_id: stream_a_id, serial: 0, payload: "Stream A Event 1".to_string() },
        Event{stream_id: stream_a_id, serial: 1, payload: "Stream A Event 2".to_string() },
        Event{stream_id: stream_a_id, serial: 2, payload: "Stream A Event 3".to_string() }
    ];
    let events_b = vec![
        Event{stream_id: stream_b_id, serial: 0, payload: "Stream B Event 1".to_string() },
        Event{stream_id: stream_b_id, serial: 1, payload: "Stream B Event 2".to_string() },
        Event{stream_id: stream_b_id, serial: 2, payload: "Stream B Event 3".to_string() },
    ];

    let mut all_events = Vec::new();
    all_events.extend(events_a.clone());
    all_events.extend(events_b.clone());

    temp_database.database.write_events(&all_events).unwrap();

    let read_events_a = temp_database.database.read_stream(stream_a_id).unwrap();
    let read_events_b = temp_database.database.read_stream(stream_b_id).unwrap();
    assert_eq!(read_events_a, events_a);
    assert_eq!(read_events_b, events_b);
}

#[test]
fn unicode_events_are_correctly_encoded() {
    let mut temp_database = create_temp_database();

    let stream_id = Uuid::parse_str("925cc5ee-f83a-4681-8094-4fb5cfe357cc").unwrap();
    let events = vec![
        Event{stream_id: stream_id, serial: 0, payload: "Hellö Wörld! 👋".to_string() },
        Event{stream_id: stream_id, serial: 1, payload: "Another Event...".to_string() },
        Event{stream_id: stream_id, serial: 2, payload: "...and a third one".to_string() }
    ];

    temp_database.database.write_events(&events).unwrap();

    let read_events = temp_database.database.read_stream(stream_id).unwrap();
    assert_eq!(read_events, events);
}
