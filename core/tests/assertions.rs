/* Copyright 2020 Lucas Hinderberger
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

macro_rules! assert_discriminant_eq {
    ($a:expr, $b:expr) => { assert_eq!(std::mem::discriminant($a), std::mem::discriminant($b))}
}