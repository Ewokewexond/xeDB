/* Copyright 2020 Lucas Hinderberger
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#[macro_use]
mod assertions;

use std::fs::File;
use std::path::Path;
use tempfile::tempdir;
use xedb_core::Database;

#[test]
fn database_can_be_created_in_empty_path() {
    let temp_dir = tempdir().unwrap();
    let database = Database::create(temp_dir.path()).unwrap();

    assert_eq!(database.path(), temp_dir.path());
}

#[test]
fn database_cannot_be_created_in_non_empty_path() {
    let temp_dir = tempdir().unwrap();
    touch(temp_dir.path().join("some_file").as_path());

    let error = Database::create(temp_dir.path()).unwrap_err();
    assert_discriminant_eq!(&error, &xedb_core::Error::TargetPathNotEmpty);
}

fn touch(path: &Path) {
    File::create(path).unwrap();
}