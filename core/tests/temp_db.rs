/* Copyright 2020 Lucas Hinderberger
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use tempfile::{ TempDir, tempdir };
use xedb_core::Database;

pub struct TempDatabase {
    pub temp_dir: TempDir,
    pub database: Database
}

pub fn create_temp_database() -> TempDatabase {
    let temp_dir = tempdir().unwrap();
    let database = Database::create(temp_dir.path()).unwrap();

    TempDatabase{temp_dir, database}
}