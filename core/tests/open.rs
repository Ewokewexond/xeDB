/* Copyright 2020 Lucas Hinderberger
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#[macro_use]
mod assertions;

mod temp_db;

use temp_db::create_temp_database;
use tempfile::tempdir;
use xedb_core::Database;

#[test]
fn existing_database_can_be_opened() {
    let path = create_temp_database().database.path().to_path_buf();
    let database = Database::open(&path).unwrap();

    assert_eq!(database.path(), path);
}

#[test]
fn database_cannot_be_opened_twice() {
    let temp_database = create_temp_database();
    let error = Database::open(temp_database.database.path()).unwrap_err();
    assert_discriminant_eq!(&error, &xedb_core::Error::DatabaseLocked);
}

#[test]
fn open_on_non_existing_path_yields_error() {
    let temp_dir = tempdir().unwrap();
    let non_existing_path = temp_dir.path().join("/doesntexist");

    let error = Database::open(&non_existing_path).unwrap_err();

    assert_discriminant_eq!(&error, &xedb_core::Error::PathDoesNotExist);
}

#[test]
fn open_on_empty_path_yields_error() {
    let temp_dir = tempdir().unwrap();
    let error = Database::open(temp_dir.path()).unwrap_err();
    assert_discriminant_eq!(&error, &xedb_core::Error::DatabaseNotFound);
}

#[test]
fn open_on_malformed_database_yields_error() {
    unimplemented!();
}

#[test]
fn open_on_wrong_database_version_yields_error() {
    unimplemented!();
}
