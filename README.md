<div align="center" style="text-align: center; margin-bottom: 50px;">

# ![xeDB](doc/logo-100px.png)
An experimental Event Sourcing database

</div>



**This is an incomplete Work in Progress**

## Summary
`xeDB` is an experimentation platform for creating a specialized database
for Event Sourcing applications.

In its first iteration, it aims to become a very minimal implementation
of an Event Sourcing database, focused on write operations and conciously
lacking sophisticated optimizations and caching, just to set a baseline.

The iterations following the initial experimental release are for incrementally
improving and optimizing `xeDB`, measuring and comparing its performance with
the previous versions.

The long-term goal is to develop a well-written, secure and fast database,
specialized for Event Sourcing. One day, `xeDB` might even reach a level of
stability, maturity and performance that it will be good enough for production use.


## Usage
Currently, `xeDB` is not fit for practical use.
Any non-experimental use, especially use in production, is strongly discouraged.

For development, `xeDB` uses Rust as its programming language and Cargo as its
build tool and package manager.


## Versioning + Releases
- The latest release is: `none`
- The release currently in development is: `v0.1.0 - The baseline`

`xeDB` uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

For details on the individual releases and the changes between them, please
read the [release log](./RELEASES.md).


## License
The `xeDB` software package is licensed under the MPLv2 license.

For details, look into the `LICENSE` file in the root of the source code
distribution of this software.
