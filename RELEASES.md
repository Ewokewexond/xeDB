# xeDB Release Log

## In Development: v0.1.0 - "The baseline"
### Summary
This initial version of `xeDB` is intended to be a minimal implementation
of an Event Sourcing database, focused on write operations and conciously
lacking sophisticated optimizations and caching.

Its purpose is to set a baseline for future incremental improvements and
optimizations to compare against.

As such, it will lack a lot of features and optimizations that would be
necessary for any non-experimental productive use.

### Some expected limitations
- Low read performance, due to the omission of a read cache and the interface
  only offering the ability to read an event stream in its entirety
- Indexes will not be saved to disk but held in memory, resulting in
  a much simpler architecture but much lower start-up speed, especially
  for larger databases
- No interface for database maintenance (deleting/redacting events or streams)
- Only a very basic REST-style HTTP interface for the server, without
  authentication or encryption
- Only the most basic tests will be included. There will for example be a lack
  of tests for high-volume and low-latency scenarios, as well as for larger
  databases. There also will be no fuzzing or property-based tests at this point.
- While the write operations are intended to be ACID (atomic, consistent,
  isolated and durable), there are no tests for this at this point.